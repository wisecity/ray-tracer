#include "RayTracer.h"

using namespace std;

Camera camera;
Light ambient_light;
vector<Light> lights;
vector<Pigment> pigments;
vector<SurfaceFinish> surface_finishes;
vector<Sphere> spheres;

string output_file_name;
float width, height;


bool is_shadowed(Ray light_ray, Sphere controlling_sphere) {
    
    bool shadowed = false;
    
    float controlling_sphere_t = controlling_sphere.get_intersect(light_ray);
    
    for (Sphere sphere : spheres) {
    
        float t = sphere.get_intersect(light_ray);
        
        if (t < controlling_sphere_t) {
            shadowed = true;
        }
        
    }
    
    return shadowed;
    
}


Pigment phong(Light light, Ray ray, Sphere intersecting_sphere, float nearest_t) {
    
    float diffuse_coefficent = intersecting_sphere.surface_finish.diffuse_coefficent;
    float specular_coefficent = intersecting_sphere.surface_finish.specular_coefficent;
    int shininess = intersecting_sphere.surface_finish.shininess;
    Vec diffuse, specular;
    
    Vec intersect_point = ray.origin + ray.direction.multiply(nearest_t);
    
    Vec surface_normal = (intersect_point - intersecting_sphere.center).normalize();
    
    if (intersecting_sphere.camera_inside_sphere) {
        surface_normal = surface_normal.negative();
    }
    
    Vec light_source_vector = (light.position - intersect_point).normalize();
    
    Ray light_ray = Ray();
    
    light_ray.origin = light.position;
    
    light_ray.direction = light_source_vector.negative();
    
    if (is_shadowed(light_ray, intersecting_sphere))
        return Vec(0, 0, 0);
     
    Vec view = (ray.origin - intersect_point).normalize();
    
    Vec halfway = (light_source_vector + view).normalize();
    
    float specularity = pow(fmaxf(0, halfway.dot(surface_normal)), shininess);

    float doIntersect = fmaxf(0, light_source_vector.dot(surface_normal));
    
    diffuse = light.intensity * intersecting_sphere.pigment.multiply(diffuse_coefficent).multiply(doIntersect);
    
    specular = light.intensity.multiply(specular_coefficent).multiply(specularity);
    
    float x_diff = light.position.x - intersect_point.x;
    
    float y_diff = light.position.y - intersect_point.y;
    
    float z_diff = light.position.z - intersect_point.z;
    
    float distance = sqrt(x_diff * x_diff + y_diff * y_diff + z_diff * z_diff);
    
    float attenuation = light.attenuation.x + light.attenuation.y * distance + light.attenuation.z * distance * distance;
    
    return (diffuse + specular) / Vec(attenuation, attenuation, attenuation);
    
}


Pigment ray_trace(Ray ray, int depth) {
    
    if (depth < 1) {
        return Pigment(0.0, 0.0, 0.0);
    }
    
    float nearest_t = FLT_MAX;
    Pigment pigment = Pigment(0.0, 0.0, 0.0);
    Sphere intersecting_sphere;
    bool intersects = false;
    
    for (Sphere sphere : spheres) {
    
        float t = sphere.get_intersect(ray);

        if (t < nearest_t) {
            nearest_t = t;
            intersecting_sphere = sphere;
            intersects = true;
        }
        
    }
    
    if (!intersects)
        return Pigment(0.5, 0.5, 0.5);
    
    float ambient_coefficent = intersecting_sphere.surface_finish.ambient_coefficent;
    Vec ambient = ambient_light.intensity * intersecting_sphere.pigment.multiply(ambient_coefficent);
    
    for (Light light : lights) {
        pigment = pigment + phong(light, ray, intersecting_sphere, nearest_t);
    }

    Vec intersect_point = ray.origin + ray.direction.multiply(nearest_t);
    Vec surface_normal = (intersect_point - intersecting_sphere.center).normalize();
    
    if (intersecting_sphere.camera_inside_sphere) {
        surface_normal = surface_normal.negative();
    }
    
    Vec r = ray.direction.negative();
    
    Vec reflection = surface_normal.multiply(2 * r.dot(surface_normal)) - r;
    
    Ray reflected_ray = Ray(intersect_point, reflection);
    reflected_ray.direction = reflection;
    reflected_ray.origin = intersect_point + reflection.multiply(0.01);
    
    float kr = intersecting_sphere.surface_finish.reflectivity_coefficent;
    Pigment reflected_color = ray_trace(reflected_ray, depth - 1).multiply(kr);
    
    return pigment + reflected_color + ambient;
    
}


void read_file(string file_name) {
    
    ifstream input_file(file_name);
    string line;

    if (input_file.is_open()) {
        getline (input_file, line);
        output_file_name = line;
        
        getline (input_file, line);
        width = stof(line.substr(0, line.find(" ")));
        height = stof(line.substr(line.find(" ")));
        
        read_camera(input_file, camera);
        
        getline(input_file, line);
        int number_of_light_sources = stoi(line);
        
        read_ambient_light(input_file ,ambient_light);
        
        read_light_sources(input_file, lights, number_of_light_sources - 1);
        
        getline(input_file, line);
        int number_of_pigments = stoi(line);
        
        read_pigments(input_file, pigments, number_of_pigments);
        
        getline(input_file, line);
        int number_of_surface_finishes = stoi(line);
        
        read_surface_finishes(input_file, surface_finishes, number_of_surface_finishes);
        
        getline(input_file, line);
        int number_of_spheres = stoi(line);
        
        read_spheres(input_file, spheres, pigments, surface_finishes, number_of_spheres);
        
        input_file.close();
    }
     
}

int main(int argc, const char * argv[]) {
    
    read_file(argv[1]);

    ofstream img(output_file_name);
    img << "P6" << endl;
    img << width << " " << height << endl;
    img << 255 << endl;
    
    float fovy = camera.fovy * PI / 180.0;
    
    float h = 2 * tan(fovy / 2);
    float a = width / height;
    float w = h * a;
    
    for (float y = 0; y < height; y++) {
        
        for (float x = 0; x < width; x++) {
            
            float px =  w * x / width - w / 2;
            float py = -h * y / height + h / 2;
            
            Vec pixel_pos = camera.position + camera.x.multiply(px) + camera.y.multiply(py) + camera.z.multiply(-1);
            
            Ray ray = Ray(camera.position, pixel_pos);
            
            Pigment pigment = ray_trace(ray, 4);
            
            pigment.x = fminf(pigment.x, 1);
            pigment.y = fminf(pigment.y, 1);
            pigment.z = fminf(pigment.z, 1);
            
            img << (unsigned char) (int) (pigment.x * 255);
            img << (unsigned char) (int) (pigment.y * 255);
            img << (unsigned char) (int) (pigment.z * 255);
        }
    
    }
    
    string open_command = "open " + output_file_name;
    system(open_command.c_str());
    
    return 0;
    
}

