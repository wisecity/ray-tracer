#include <fstream>
#include <sstream>
#include <iostream>
#include <cmath>
#include <cfloat>
#include <vector>
#include <regex>

#define PI 3.14159265
#define EPSILON 0.001

using namespace std;

typedef struct Vec {
    
    float x, y, z;
    
    Vec() {}
    
    Vec(float x, float y, float z) {
        this->x = x;
        this->y = y;
        this->z = z;
    }
    
    Vec(Vec origin, Vec direction) {
        x = direction.x - origin.x;
        y = direction.y - origin.y;
        z = direction.z - origin.z;
    }
    
    Vec normalize() {
        float length = sqrt(x * x + y * y + z * z);
        return Vec(x / length, y / length, z / length);
    }
    
    Vec operator - (Vec v) { return Vec(x-v.x, y-v.y, z-v.z); }
    
    Vec operator + (Vec v) { return Vec(x+v.x, y+v.y, z+v.z); }
    
    Vec operator * (Vec v) { return Vec(x*v.x, y*v.y, z*v.z); }
    
    Vec operator / (Vec v) { return Vec(x/v.x, y/v.y, z/v.z); }
    
    
    Vec negative() { return Vec(-x, -y, -z); }
    
    Vec multiply(float k) { return Vec(x*k, y*k, z*k); }
    
    float dot(Vec v) { return x * v.x + y * v.y + z * v.z; }
    
    Vec cross(Vec v) { return Vec(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x); }
    
} Vec3;

typedef struct Camera {
    
    Vec position, at, up;
    Vec x, y, z;
    float fovy;
    
    Camera() {}

    Camera(Vec position, Vec at, Vec up, float fovy) {
        this->position = position;
        this->at = at;
        this->up = up;
        this->fovy = fovy;
        z = (at - position).normalize().negative();
        x = up.cross(z).normalize();
        y = z.cross(x);
    }
    
} Camera;

typedef struct Ray {
    
    Vec origin, direction;
    
    Ray() {}
    
    Ray(Vec origin, Vec pixel_loc) {
        this->origin = origin;
        this->direction = (pixel_loc - origin).normalize();
    }
    
} Ray;

typedef struct Light{
    
    Vec position, intensity, attenuation;
    
    Light() {}
    
    Light(Vec position, Vec intensity, Vec attenuation) {
        this->position = position;
        this->intensity = intensity;
        this->attenuation = attenuation;
    }
    
} Light;

typedef Vec Pigment;

typedef struct SurfaceFinish{
    
    float ambient_coefficent, diffuse_coefficent, specular_coefficent, reflectivity_coefficent;
    int shininess;
    
    SurfaceFinish() {}
    
    SurfaceFinish(float ambient_coefficent, float diffuse_coefficent, float specular_coefficent,
                  int shininess, float reflectivity_coefficent) {
        
        this->ambient_coefficent = ambient_coefficent;
        this->diffuse_coefficent = diffuse_coefficent;
        this->specular_coefficent = specular_coefficent;
        this->shininess = shininess;
        this->reflectivity_coefficent = reflectivity_coefficent;
    
    }
    
    
} SurfaceFinish;

typedef struct Sphere{
    
    Vec center, pigment;
    float radius;
    SurfaceFinish surface_finish;
    bool camera_inside_sphere;
    
    Sphere(){}
    
    Sphere(Vec center, float radius, Vec pigment, SurfaceFinish surface_finish) {
        this->center = center;
        this->radius = radius;
        this->pigment = pigment;
        this->surface_finish = surface_finish;
        camera_inside_sphere = false;
    }
    
    float get_intersect(Ray r) {
        
        Vec u = center - r.origin;
        Vec d = r.direction;
        
        float b = -2 * d.dot(u);
        float c = u.dot(u) - radius * radius;
        
        float discriminant = b * b - 4 * c;

        if (discriminant < -EPSILON)
            return FLT_MAX;
        else {
            float root1 = (-b + sqrt(discriminant)) / 2;
            float root2 = (-b - sqrt(discriminant)) / 2;
            
            if (root1 < 0 && root2 < 0)
                return FLT_MAX;
            
            if (root1 < 0 || root2 < 0) {
                camera_inside_sphere = true;
                return fmaxf(root1, root2);
            }
            
            return fminf(root1, root2);
        }
    }
    
} Sphere;

void split(const string& str, vector<float>& splitted, char delim) {
    stringstream ss(str);
    string token;
    while (getline(ss, token, delim)) {
        splitted.push_back(stof(token));
    }
}

void read_camera(ifstream &input_file, Camera &camera) {
    
    if (input_file.is_open()) {
     
        string line;
        vector<float> splitted;
        
        getline (input_file, line);
        line = regex_replace(line, regex("\\s+"), " ");
        split(line, splitted, ' ');
        
        Vec pos(splitted.at(0), splitted.at(1), splitted.at(2));
        
        getline (input_file, line);
        line = regex_replace(line, regex("\\s+"), " ");
        split(line, splitted, ' ');
        
        Vec at(splitted.at(3), splitted.at(4), splitted.at(5));
        
        getline (input_file, line);
        line = regex_replace(line, regex("\\s+"), " ");
        split(line, splitted, ' ');
        
        Vec up(splitted.at(6), splitted.at(7), splitted.at(8));
        
        getline (input_file, line);
        float fovy = stof(line);
        
        camera = Camera(pos, at, up, fovy);
        
    }
}

void read_ambient_light(ifstream &input_file, Light &ambient_light) {
    if (input_file.is_open()) {
        string line;
        vector<float> splitted;
     
        getline (input_file, line);
        line = regex_replace(line, regex("\\s+"), " ");
        split(line, splitted, ' ');
     
        Vec pos(splitted.at(0), splitted.at(1), splitted.at(2));
        Vec intensity(splitted.at(3), splitted.at(4), splitted.at(5));
        Vec attenuation(splitted.at(6), splitted.at(7), splitted.at(8));
        
        ambient_light = Light(pos, intensity, attenuation);
    }
}

void read_light_sources(ifstream &input_file, vector<Light> &lights, int number_of_lights) {
    
    if (input_file.is_open()) {
     
        for (int i = 0; i < number_of_lights; i++) {
            string line;
            vector<float> splitted;
            
            getline (input_file, line);
            line = regex_replace(line, regex("\\s+"), " ");
            split(line, splitted, ' ');
            
            Vec pos(splitted.at(0), splitted.at(1), splitted.at(2));
            Vec intensity(splitted.at(3), splitted.at(4), splitted.at(5));
            Vec attenuation(splitted.at(6), splitted.at(7), splitted.at(8));
            
            lights.push_back(Light(pos, intensity, attenuation));
        }
    }
    
}

void read_pigments(ifstream &input_file, vector<Pigment> &pigments, int number_of_pigments) {
    
    if (input_file.is_open()) {
     
        for (int i = 0; i < number_of_pigments; i++) {
            string line;
            vector<float> splitted;
            
            getline (input_file, line);
            line = regex_replace(line, regex("\\s+"), " ");
            split(line.substr(line.find(" ") + 1), splitted, ' ');
            
            pigments.push_back(Pigment(splitted.at(0), splitted.at(1), splitted.at(2)));
        }
    
    }
    
}

void read_surface_finishes(ifstream &input_file, vector<SurfaceFinish> &surface_finishes, int number_of) {
    
    if (input_file.is_open()) {
     
        for (int i = 0; i < number_of; i++) {
            string line;
            vector<float> splitted;
            
            getline (input_file, line);
            line = regex_replace(line, regex("\\s+"), " ");
            split(line, splitted, ' ');
            
            SurfaceFinish surfaceFinish(splitted.at(0), splitted.at(1), splitted.at(2), splitted.at(3), splitted.at(4));
            
            surface_finishes.push_back(surfaceFinish);
        }
    
    }
    
}

void read_spheres(ifstream &input_file, vector<Sphere> &spheres, vector<Pigment> &pigments, vector<SurfaceFinish> &surface_finishes, int number_of_spheres) {
    
    if (input_file.is_open()) {
     
        for (int i = 0; i < number_of_spheres; i++) {
            string line;
            vector<float> splitted;
            
            getline (input_file, line);
            line = regex_replace(line, regex("\\s+"), " ");
            split(line.substr(0, line.find("sphere")), splitted, ' ');
            
            int pigment_number = splitted.at(0);
            int surface_finish_number = splitted.at(1);
            
            splitted.clear();
            split(line.substr(line.find("sphere ") + 7), splitted, ' ');
            
            Vec pos(splitted.at(0), splitted.at(1), splitted.at(2));
            float radius = splitted.at(3);
            
            Sphere sphere(pos, radius, pigments.at(pigment_number), surface_finishes.at(surface_finish_number));
            
            spheres.push_back(sphere);
        }
    
    }
    
}
